// 页面数据
var data = {
	listData: [

	],
	engineList: {
		targetIndex: 0,
		list: [{
				name: "必应",
				url: "https://cn.bing.com/search?q=%s",
			},
			{
				name: "百度",
				url: "https://www.baidu.com/s?wd=%s",
			},
			{
				name: "谷歌",
				url: "https://www.google.com.hk/search?q=%s",
			},
			{
				name: "维基百科",
				url: "https://zh.wikipedia.org/wiki/%s",
			},
		],
	},
	diaryList: [{
		date: "2021-06-20 18:02",
		title: "日记",
		content: "记录你的每一天！",
	}, ],
};

/* 
// 第一层列表
var ulListData = {
	title:"",
	subListData:[
		
	],
};
// 第二层列表
 var subLiData = {
	url:"",
	src:"",
	name:"",
} 

var engineList = {
		targetIndex: 0,
		list: [
			{
			name: "必应",
			url: "https://cn.bing.com/search?q=%s",
			}, 
		],
	}
var diaryList = [
	{
		date: "",
		title:"",
		content: "",
	},
];
*/


// 定义日期格式
Date.prototype.format = function(fmt) {
	var o = {
		"M+": this.getMonth() + 1, //月份
		"d+": this.getDate(), //日
		"h+": this.getHours() % 12 == 0 ? 12 : this.getHours() % 12, //小时
		"H+": this.getHours(), //小时
		"m+": this.getMinutes(), //分
		"s+": this.getSeconds(), //秒
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度
		"S": this.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt))
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o)
		if (new RegExp("(" + k + ")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k])
				.length)));
	return fmt;
}



//定义全局变量函数
var localStorage = window.localStorage;
//定义全局变量
var keydata = "keydata";

//设置缓存
function setLocalStorage(key, value) {
	var v = value;
	//是对象转成JSON，不是直接作为值存入内存
	if (typeof v == 'object') {
		v = JSON.stringify(v);
		v = 'obj-' + v;
	} else {
		v = 'str-' + v;
	}
	var localStorage = window.localStorage;
	if (localStorage) {
		localStorage.setItem(key, v);
	}
};
//获取缓存
function getLocalStorage(key) {
	var localStorage = window.localStorage;
	if (localStorage)
		var v = localStorage.getItem(key);
	if (!v) {
		return;
	}
	if (v.indexOf('obj-') === 0) {
		v = v.slice(4);
		return JSON.parse(v);
	} else if (v.indexOf('str-') === 0) {
		return v.slice(4);
	}
};

// 封装推荐数据写入方法
function setData() {
	setLocalStorage(keydata, data);
}



var kdata = getLocalStorage(keydata);
console.log(kdata);
if (kdata != null) {
	data = kdata;
	if (!data.hasOwnProperty("engineList")) {
		data["engineList"] = {
			targetIndex: 0,
			list: [{
					name: "必应",
					url: "https://cn.bing.com/search?q=%s",
				},
				{
					name: "百度",
					url: "https://www.baidu.com/s?wd=%s",
				},
				{
					name: "谷歌",
					url: "https://www.google.com.hk/search?q=%s",
				},
				{
					name: "维基百科",
					url: "https://zh.wikipedia.org/wiki/%s",
				},
			],
		}
	}
	if (!data.hasOwnProperty("diaryList")) {
		data["diaryList"] = [{
			date: "2021-06-20 18:02",
			title: "日记",
			content: "记录你的每一天！",
		}];
	}
}


// 搜索

function writeEngine(i) {
	var ul = document.querySelector(".search .engine-list");
	var ct = "";
	if (i == data.engineList.targetIndex) {
		ct = "class=\"up\"";
		document.querySelector("#search-input").placeholder = "搜索网页-" + data.engineList.list[i].name;
	}
	var li = "<li onclick=\"upEngine(this)\"><p " + ct + ">" + data.engineList.list[i].name + "</p></li>";
	ul.innerHTML += li;
}

function goSearch() {
	var content = document.querySelector("#search-input").value;
	if (content) {
		url = data.engineList.list[data.engineList.targetIndex].url.replace("%s", content);
		window.open(url, "newW");
	}
}

function openEngineList(v) {
	var ul = v.nextElementSibling || v.nextSibling;
	if (ul.style.display == "block") {
		ul.style.animation = "yinchen 0.2s";
		setTimeout(function() {
			ul.style.display = "";
			ul.style.animation = "";
		}, 200);
	} else {
		ul.style.display = "block";
	}

}

function upEngine(v) {
	if (v.children[0].getAttribute("class") == "up") {
		v.parentNode.style.animation = "yinchen 0.2s";
		setTimeout(function() {
			v.parentNode.style.display = "";
			v.parentNode.style.animation = "";
		}, 200);
		return;
	}
	var ul = v.parentNode.children;
	for (let i = 0; i < ul.length; i++) {
		if (ul[i] == v) {
			data.engineList.targetIndex = i;
			setData();
			v.children[0].classList = "up";
			v.parentNode.style.animation = "yinchen 0.2s";
			document.querySelector("#search-input").placeholder = "搜索网页-" + data.engineList.list[i].name;
			setTimeout(function() {
				v.parentNode.style.display = "";
				v.parentNode.style.animation = "";
			}, 200);
		} else {
			ul[i].children[0].classList.remove("up");
		}
	}
}





// 写入数据
function addItem(title) {
	var ulListData = {
		title: title,
		subListData: [

		],
	};
	data.listData.push(ulListData);
	setData();
}

// 写入块
function writeUl(i) {
	var subList = document.querySelector(".sub-list .sub-list-item-add");
	var ul = document.querySelector(".sub-list");
	var div = document.createElement("div");
	// var i = data.listData.length - 1;
	var li = "<li class=\"sub-list-item\" index=" + i + "><ul class=\"sub-item-list\">" +
		"<li><div class=\"sub-item-content\" onclick=\"openAddSubItem(this)\">" +
		"<span class=\"add-item\">+</span ></div></li></ul>" +
		"<span class=\"sub-item-title\">" + data.listData[i].title + "</span>" +
		"<div class=\"item-menu\"><p class=\"sub-item-toast\" onclick=\"itemRename(this)\"></p>" +
		"<p class=\"sub-item-toast\" onclick=\"itemRemove(this)\"></p><p class=\"toast\"></div></p></li>";
	div.innerHTML = li;
	li = div.querySelector("li");
	ul.insertBefore(li, subList);
}

// 写入第二层item
function writeSubUl(index, i) {
	var subList = document.querySelectorAll(".sub-item-list")[index];
	var subli = subList.querySelectorAll("li");
	var div = document.createElement("div");
	var subLiData = data.listData[index].subListData[i];
	var li = "<li class=\"sub-item-list-item\" index=" + i + ">" +
		"<div class=\"sub-item-content\">" +
		"<button onclick=\"subItemRemove(this)\"></button>" +
		"<button onclick=\"subItemCompile(this)\"></button><p></p>" +
		"<img onclick=openPage(\"" + subLiData.url + "\") src=\"" + subLiData.src + "\" ><span>" + subLiData.name +
		"</span></div></li>";
	div.innerHTML = li;
	li = div.querySelector("li");
	subList.insertBefore(li, subli[subli.length - 1]);
}


// 第二层item加入到数据
function addSubItem(index, url, src, name) {
	var subLiData = {
		url: url,
		src: src,
		name: name,
	};
	if (data.listData.length <= index || data.listData.length == 0 || index < 0) {
		console.log()("addSubItem--index超出边界/没创建块。")
		return;
	}
	data.listData[index].subListData.push(subLiData);
	setData();
}






// 打开添加第一层item
function openAddPiece() {
	var title = window.prompt("添加", "空白页");
	if (title != null && title.length != "") {
		addItem(title);
		writeUl(data.listData.length - 1);
	}
}


// 第一层li删除
function itemRemove(v) {
	var isR = window.confirm("确认删除！");
	if (!isR) {
		return;
	}
	var li = v.parentNode.parentNode;
	data.listData.splice(li.getAttribute("index"), 1);
	setData();
	var ul = document.querySelector(".sub-list");
	ul.removeChild(li);
	var subList = document.querySelectorAll(".sub-list .sub-list-item");
	subList.forEach((v, i) => {
		v.setAttribute("index", i);
	});
}

// 第一层li重命名
function itemRename(v) {
	var li = v.parentNode.parentNode;
	var span = v.parentNode.previousElementSibling || v.previousSibling;
	var title = window.prompt("重命名", span.innerText);
	if (title != null) {
		span.innerText = title;
		data.listData[li.getAttribute("index")].title = title;
		setData();
	}
}


// 打开添加第二层item
function openAddSubItem(v) {
	var i = v.parentNode.parentNode.parentNode.getAttribute("index");
	var shade = document.querySelector(".content-center .shade");
	var shadediv = shade.querySelector("div");
	shadediv.setAttribute("i", i);
	shadediv.setAttribute("j", -1);
	shade.style.visibility = "visible";
	shade.querySelector(".content-dialog").style.display = "block";
}



// 保存弹窗

// 取消
function shadeCancel(v) {
	var div = v.parentNode.parentNode;
	div.setAttribute("i", "-1");
	div.setAttribute("j", "-1");
	div.parentNode.style.visibility = "";
	div.parentNode.querySelector(".content-dialog").style.display = "";
}


// 获取网站图标
function getIcon(url) {
	var domain = url.split("/"); //以“/”进行分割
	if (domain[2]) {
		domain = "http://" + domain[2] + "/favicon.ico";
	} else {
		domain = 'img/ic_gamecenter.png'; //如果url不正确就取空
	}
	return domain;
}



// 确认
function shadeDone(v) {
	var div = v.parentNode.parentNode;
	var i = div.getAttribute("i");
	var j = div.getAttribute("j");
	var name = document.querySelector("#shade-name").value;
	var url = document.querySelector("#shade-url").value;
	var src = getIcon(url);
	if (url != "") {
		if (i != -1 && j == -1) {
			addSubItem(i, url, src, name);
			writeSubUl(i, data.listData[i].subListData.length - 1);
		} else if (i != -1 && j != -1) {
			data.listData[i].subListData[j].url = url;
			data.listData[i].subListData[j].src = src;
			data.listData[i].subListData[j].name = name;
			setData();
			var div1 = document.querySelectorAll(".sub-list li")[i].querySelectorAll("ul li")[j].querySelector("div");
			var img = div1.querySelector("img");
			img.setAttribute("onclick", "openPage(\"" + url + "\")");
			img.src = src;
			var span = div1.querySelector("span");
			span.innerText = name;
		} else {
			alert("出错！");
		}
		div.parentNode.style.visibility = "";
	} else {
		alert("链接不可为空！");
	}

}




// 第二层li删除
function subItemRemove(v) {
	var isR = window.confirm("确认删除！");
	if (!isR) {
		return;
	}
	var i = v.parentNode.parentNode.parentNode.parentNode.getAttribute("index");
	var li = v.parentNode.parentNode;
	var ul = li.parentNode;
	var j = li.getAttribute("index");
	data.listData[i].subListData.splice(j, 1);
	setData();
	ul.removeChild(li);
	var subList = ul.querySelectorAll(".sub-item-list-item");
	subList.forEach((v, i) => {
		v.setAttribute("index", i);
	});
}

function subItemCompile(v) {
	var i = v.parentNode.parentNode.parentNode.parentNode.getAttribute("index");
	var li = v.parentNode.parentNode;
	var j = li.getAttribute("index");
	var shade = document.querySelector(".content-center .shade");
	var shadediv = shade.querySelector("div");
	document.querySelector(".content-dialog img").src = data.listData[i].subListData[j].src;
	document.querySelector("#shade-name").value = data.listData[i].subListData[j].name;
	document.querySelector("#shade-url").value = data.listData[i].subListData[j].url;
	shadediv.setAttribute("i", i);
	shadediv.setAttribute("j", j);
	shade.style.visibility = "visible";
	shade.querySelector(".content-dialog").style.display = "block";
}

function openPage(url) {
	window.open(url, "newW");
}



// 左边区域

var isClick = false;
var clickIndex = -1;

// 添加日记数据
function addDiary(title, content) {
	var diary = {
		date: new Date().format('yyyy-MM-dd hh:mm:ss'),
		title: title,
		content: content,
	}
	data.diaryList.unshift(diary);
	setData();
}

// 添加日记到html
function writeDiaryLi(i) {
	var ul = document.querySelector(".content-left ul");
	var diarylist = data.diaryList[i];
	var title = diarylist.title;
	if (title == "") {
		title = diarylist.content;
	}
	var li = "<li onmouseenter=\"onDiaryListEnter(this)\" " +
		" onmouseleave=\"onDiaryListLeave(this)\"><span>" + diarylist.date + "</span>" +
		"<span>" + title + "</span><li>";
	var div = document.createElement("div");
	div.innerHTML = li;
	li = div.children[0];
	if (ul.length == 1) {
		ul.appendChild(li);
	} else {
		ul.insertBefore(li, ul.children[1]);
	}
	li.onclick = function(e){
		onDiaryListClick(this);
		e.stopPropagation();
	}
}

// 设置编辑框内容
function setDiaryText(title, content) {
	document.querySelector("#diary-title").value = title;
	document.querySelector("#diary-content").value = content;
}

// 控制编辑框是否可编辑
function diaryIsEdit(isEdit) {
	var title = document.querySelector("#diary-title");
	var content = document.querySelector("#diary-content");
	if (!isEdit) {
		title.readOnly = "readonly";
		content.readOnly = "readonly";
	} else {
		title.readOnly = "";
		content.readOnly = "";
	}
}

// 点击日记条目
function onDiaryListClick(v) {
	var liList = v.parentNode.children;
	var index = -1;
	diaryIsEdit(false);
	for (let i = 1; i < liList.length; i++) {
		if (v == liList[i]) {
			index = i;
		}
	}
	if (clickIndex == -1) {
		liList[index].style.backgroundColor = "rgb(80, 80, 80, 0.3)";
	} else if (clickIndex != index) {
		liList[clickIndex].style.backgroundColor = "";
		liList[index].style.backgroundColor = "rgb(80, 80, 80, 0.3)";
		document.querySelector(".diary").style.animation = "diary_visible 0.4s";
	}
	clickIndex = index;
	var diary = data.diaryList[clickIndex - 1];
	setDiaryText(diary.title, diary.content);
	isClick = true;
}

// 点击添加li
function onDiaryListAddClick(v) {
	diaryIsEdit(true);
	if (clickIndex != -1) {
		v.parentNode.children[clickIndex].style.backgroundColor = "";
	}
	document.querySelector(".diary").style.visibility = "visible";
	document.querySelector(".diary").style.animation = "diary_visible 0.4s";
	v.parentNode.children[0].style.backgroundColor = "rgb(80, 80, 80, 0.3)";
	setDiaryText("", "");
	clickIndex = 0;
	isClick = true;
}

// 鼠标进来执行动画
function onDiaryListEnter(v) {
	if (!isClick) {
		for (let i = 0; i < v.parentNode.children.length; i++) {
			if (v == v.parentNode.children[i]) {
				var diary = data.diaryList[i - 1];
				setDiaryText(diary.title, diary.content);
				break;
			}
		}
		var diary = document.querySelector(".diary");
		diary.style.animation = "diary_visible 0.4s";
		diary.style.visibility = "visible";
	}
}


// 鼠标li离开执行动画
function onDiaryListLeave(v) {
	if (!isClick) {
		var diary = document.querySelector(".diary");
		diary.style.animation = "diary_hidden 0.4s";
	}
}

// 动画结束触发
document.querySelector(".diary").onanimationend = function(e) {
	if (e.animationName == "diary_hidden") {
		e.target.style.visibility = "hidden";
	} else {
		e.target.style.animation = "";
	}
}




// 日记菜单事件

// 关闭事件
function onDiaryClose() {
	var diary = document.querySelector(".diary");
	diary.style.animation = "diary_hidden 0.4s";
	isClick = false;
	document.querySelectorAll(".content-left ul li")[clickIndex].style.backgroundColor = "";
	clickIndex = -1;
}

// 编辑事件
function onDiaryRedact() {
	diaryIsEdit(true);
}

// 保存事件
function onDiarySave() {
	if (clickIndex == 0) {
		addDiary(document.querySelector("#diary-title").value, document.querySelector("#diary-content").value);
		writeDiaryLi(0);
		document.querySelectorAll(".content-left ul li")[clickIndex].style.backgroundColor = "";
		document.querySelectorAll(".content-left ul li")[1].style.backgroundColor = "rgb(80, 80, 80, 0.3)";
		clickIndex = 1;
		document.querySelector(".diary").style.animation = "diary_visible 0.4s";
	} else {
		data.diaryList[clickIndex - 1].date = new Date().format('yyyy-MM-dd hh:mm:ss');
		data.diaryList[clickIndex - 1].title = document.querySelector("#diary-title").value;
		data.diaryList[clickIndex - 1].content = document.querySelector("#diary-content").value;
	}
	diaryIsEdit(false);
	setData();
}


function diaryNotClick(path){
	var aaa = true;
	if(path == null){
		return aaa;
	}
	for (let i = 1; i < path.length - 5; i++) {
		let v = path[i].tagName;
		if (path[i-1].tagName == "LI" && v == "UL") {
			if(path[i + 1].classList[0] == "content-left"){
				aaa = false;
				break;
			}
		}else if(path[i].classList.length == 1 && path[i + 1].classList.length == 1){
			if (path[i].classList[0] == "diary" && path[i + 1].classList[0] == "content-left") {
				aaa = false;
				break;
			}
		}
	}
	return aaa;
}



// 阻止事件传播
document.querySelector(".diary").onclick = function(e){
	e.stopPropagation();
}


document.onclick = function(e) {
	console.log(e);
	if (isClick && diaryNotClick(e.path)) {
		isClick = false;
		document.querySelectorAll(".content-left ul li")[clickIndex].style.backgroundColor = "";
		clickIndex = -1;
		var diary = document.querySelector(".diary");
		diary.style.animation = "diary_hidden 0.4s";
	}
}


// 重载
function reload() {
	for (let i = 0; i < data.listData.length; i++) {
		let ulListData = data.listData[i];
		writeUl(i);
		for (let j = 0; j < ulListData.subListData.length; j++) {
			writeSubUl(i, j);
		}
	}

	for (let i = 0; i < data.engineList.list.length; i++) {
		writeEngine(i);
	}

	for (let i = data.diaryList.length - 1; i >= 0; i--) {
		writeDiaryLi(i);
	}
}


// 数据加载
window.onload = function() {
	reload();
	// document.querySelector("#background-video").style.display = "block";
}
